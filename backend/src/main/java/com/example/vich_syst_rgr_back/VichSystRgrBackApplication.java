package com.example.vich_syst_rgr_back;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(
        title = "Система управления банковскими операциями через банкоматы",
        version = "для сдачи 1",
        description = "систаема всключает в себя управление банкоматами, пользователями и их счетами"
))
public class VichSystRgrBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(VichSystRgrBackApplication.class, args);
    }

}
