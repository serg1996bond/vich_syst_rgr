package com.example.vich_syst_rgr_back.modules.atm.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class AtmTransferDto {
    @Min(1)
    @Schema(
            description = "счет текущего пользователя, с которого переводится сумма",
            example = "1"
    )
    private final Integer fromAccountId;
    @Min(1)
    @Schema(
            description = "счет на который переводится сумма",
            example = "2"
    )
    private final Integer toAccountId;
    @Min(0)
    @Schema(
            description = "сумма перевода",
            example = "5000"
    )
    private final BigDecimal amount;
}
