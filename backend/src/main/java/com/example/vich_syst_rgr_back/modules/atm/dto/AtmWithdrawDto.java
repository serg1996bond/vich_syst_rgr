package com.example.vich_syst_rgr_back.modules.atm.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AtmWithdrawDto {
    @Min(1)
    @Schema(
            description = "id используемого банкомата",
            example = "1"
    )
    private int atmId;
    @Min(1)
    @Schema(
            description = "id счета текущего пользователя",
            example = "1"
    )
    private int accountId;
    @Min(0)
    @Schema(
            description = "сумма, снимаемая с указанного счета",
            example = "50"
    )
    private BigDecimal toSub;
}