package com.example.vich_syst_rgr_back.modules.bank.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "history_lines")
@Setter
@Getter
public class HistoryLine {

    @Column(name = "command_type")
    @Schema(description = "тип проведенной банковской операции", example = "deposit")
    private String commandType;
    @Column(name = "date")
    @Schema(description = "дата проведения операции")
    private LocalDateTime date;
    @Column(name = "money")
    @Schema(description = "сумма проведенной операции", example = "200")
    private BigDecimal money;

}
