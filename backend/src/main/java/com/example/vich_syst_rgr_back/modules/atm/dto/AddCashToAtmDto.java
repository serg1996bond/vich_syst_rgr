package com.example.vich_syst_rgr_back.modules.atm.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddCashToAtmDto {
    @Min(1)
    @Schema(
            description = "id банкомата"
    )
    private int atmId;
    @Min(0)
    @Schema(
            description = "сумма пополнения банкомата"
    )
    private double toAdd;
}
