package com.example.vich_syst_rgr_back.modules.user.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import java.util.Set;

/**ДТО для универсального создания пользователя*/

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterUserDto {
    @Pattern(regexp = "[A-Za-z]+")
    @Schema(description = "логин пользователя", example = "fooLogin")
    private String username;
    @Schema(description = "пароль пользователя", example = "barPassword1234")
    private String password;
    @Schema(description = "роли пользователя", example = "BANK_USER")
    private Set<String> authorities;
}
