package com.example.vich_syst_rgr_back.modules.user.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Pattern;

/**
 * ДТО для регистрации пользователя банковской части приложения
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterBankUserDto {
    @Pattern(regexp = "[A-Za-z]+")
    @Schema(description = "логин пользователя", example = "fooLogin")
    private String username;
    @Schema(description = "пароль пользователя", example = "barPassword1234")
    private String password;
}
